const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.get("/", (req, res) => {
	res.send("Hello World")
});

app.post("/", (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

let users = [];

app.post("/signup", (req, res) => {
	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} is Successfuly Registered`);
	} else {
		res.send(`Please input both username and password`)
	}

	console.log(req.body);
});

app.listen(port, () => console.log(`Server is listening at localport:${port}`));

app.put("/change-password", (req, res) => {
	let message;

	for (let user = 0; user < users.length; user++) {
		if(req.body.username == users[user].username) {
			users[user].password = req.body.password;
			message = `User ${req.body.username} has Successfuly change their password`;
			break;
		} else {
			message = `User ${req.body.username} does not exist`;
		}
	}

	res.send(message);

})

// Activity s34

// Create a GET route that will access the /home route that will print out a simple message.

// Process a GET request at the /home route using postman.

	app.get("/home", (req, res) => {
		res.send(`Welcome to the Home Page!`)
	})

// Create a GET route that will access the /users route that will retrieve all the users in the mock database.

// Process a GET request at the /users route using postman.

	app.get("/users", (req, res) => {
		res.send(users);
	})

// Export the Postman collection and save it inside the root folder of our application.

// Stretch Goal:

// Create a DELETE route that will access the /delete-item route to remove a user from the mock database.

// Process a DELETE request at the /delete-item route using postman.
	
	app.delete("/delete-item", (req, res) => {
		let message;

		for (let user = 0; user < users.length; user++) {
			if(req.body.username == users[user].username) {
				users.splice(user, 1);
				message = `User ${req.body.username} has been Successfuly removed`;
				break;
			} else {
				message = `User ${req.body.username} does not exist`;
			}
		}
		res.send(message);
	})

